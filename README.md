Lexer
==
#概述
一个用Java Swing编写的C语言词法分析器的Demo项目。

词法分析是编译过程的第一个阶段，是编译的基础。这个阶段的任务是从左到右一个字符一个字符地读入源程序，即对构成源程序的字符流进行扫描然后根据构词规则识别单词(也称单词符号或符号)。

输入：一个源程序的字符串。

过程：按照C语言词法分析词素构成。

输出：1） 一个词法单元序列，每个词法单元对应一个词素；2） 一个符号表包含标识符和定义数字常量。

项目的算法基本上参考了龙书附录后面的C++代码，只是用Swing加了一层GUI。

代码写于2012年4月份的，那时候代码写的很烂，最近花了几天的时间重构了一下，把代码托管到Git@OSC上。

#项目特性

+ 支持识别十进制数、八进制数、标识符、关键字、分割符、操作符等多种词素
+ 支持*文件导入*和*代码编写*两种输入方式
+ 算法和UI通过 ```Analyzer.LexerAnalysisProccessCallback``` 实现二者之间的低耦合

#项目结构
```com.kinegratii.lexer``` 包

+ Lexer.java 项目启动类
+ MainFrame.java 主界面
+ SoftwareInfo.java 软件信息常量定义

```com.kinegratii.lexer.core``` 词法分析包

+ Analyser.java  分析器
+ LexerAnalysisProccessCallback.java 回调接口

```com.kinegratii.lexer.token``` 数据结构包

+ 定义了一系列可识别的词素类

```com.kinegratii.lexer.utils``` 工具包

+ BareBonesBrowserLaunch.java 浏览器访问


#关于作者

项目博客：[http://my.oschina.net/kinegratii/blog/322944](http://my.oschina.net/kinegratii/blog/322944)

作者：Kinegratii

个人主页：[http://my.oschina.net/kinegratii](http://my.oschina.net/kinegratii)


